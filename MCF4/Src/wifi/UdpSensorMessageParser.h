/**
 * Copyright (C) 2019 Philipp Nendel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef WIFI_UDPSENSORMESSAGEPARSER_H_
#define WIFI_UDPSENSORMESSAGEPARSER_H_

#include <stdint.h>
#include "main.h"

void Udp_parseSensorMessage(uint8_t* sensorMessage, RTC_HandleTypeDef *hrtc);

#endif /* WIFI_UDPSENSORMESSAGEPARSER_H_ */
