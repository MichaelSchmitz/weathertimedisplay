/**
 * Copyright (C) 2019 Philipp Nendel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "UdpSensorMessageParser.h"
#include "../matrix/Matrix.h"
#include <string.h>
#include <wchar.h>
#include <stdlib.h>

static wchar_t* Udp_sliceMessage(uint8_t* sensorMessage, uint8_t sensorIdLength);

RTC_TimeTypeDef sTime = {0};

/**
 * Parses a UDP sensor message based upon its first characters which indicate the sensor origin device.
 * Passes the parsed values directly to the LED matrix and RTC API.
 * @param sensorMessage The sensor message that should be parsed and used to update matrix/RTC values.
 * @param hrtc The RTC handler.
 */
void Udp_parseSensorMessage(uint8_t* sensorMessage, RTC_HandleTypeDef *hrtc) {
	wchar_t* slicedData = NULL;

	if (*sensorMessage == 'T') {
		uint8_t* secondId = sensorMessage;
		secondId++;

		if (*secondId == '1') {
			// parse 'T1'
			slicedData = Udp_sliceMessage(sensorMessage, 3);
			Matrix_setTemperature1(slicedData);
		} else if (*secondId == '2') {
			// parse 'T2'
			slicedData = Udp_sliceMessage(sensorMessage, 3);
			Matrix_setTemperature2(slicedData);
		} else if (*secondId == 'I') {
			// parse 'TIME'
			slicedData = Udp_sliceMessage(sensorMessage, 5);
			RTC_TimeTypeDef sTime = {0};
			wchar_t *end;
		    sTime.Hours = wcstol (slicedData, &end, 10);
		    end++;
		    sTime.Minutes = wcstol (end, NULL, 10);
			HAL_RTC_SetTime(hrtc, &sTime, RTC_FORMAT_BIN);
		}
	} else if (*sensorMessage == 'P') {
		uint8_t* secondId = sensorMessage;
		secondId++;
		slicedData = Udp_sliceMessage(sensorMessage, 3);

		if (*secondId == '1') {
			// parse 'P1'
			Matrix_setPressure1(slicedData);
		} else if (*secondId == '2') {
			// parse 'P2'

			Matrix_setPressure2(slicedData);
		}
	} else if (*sensorMessage == 'H') {
		uint8_t* secondId = sensorMessage;
		secondId++;
		slicedData = Udp_sliceMessage(sensorMessage, 3);

		if (*secondId == '1') {
			// parse 'H1'
			Matrix_setHumidity1(slicedData);
		} else if (*secondId == '2') {
			// parse 'H2'
			Matrix_setHumidity2(slicedData);
		}
	} else if (*sensorMessage == 'D') {
		uint8_t* secondId = sensorMessage;
		secondId++;
		slicedData = Udp_sliceMessage(sensorMessage, 5);

		if (*secondId == 'I') {
			// parse 'DIST'
			Matrix_toggleMode();
		}

		else if (*secondId == 'A') {
			// parse 'DATE'
			RTC_DateTypeDef sDate = {0};
			wchar_t *end;
		    sDate.Date = wcstol (slicedData, &end, 10);
		    end++;
		    sDate.Month = wcstol (end, &end, 10);
		    end++;
		    sDate.Year = wcstol (end, NULL, 10);
			HAL_RTC_SetDate(hrtc, &sDate, RTC_FORMAT_BIN);

		}
	}
	Matrix_redraw();
	free(slicedData);
	slicedData = NULL;

}

/**
 * Shortens the incoming UDP message to only include relevant measured sensor values.
 * @param sensorMessage The sensor message that should be shortened.
 * @param sensorIdLength The length of the sensor origin device ID within the message.
 * @return Returns a pointer to wide characters.
 */
static wchar_t* Udp_sliceMessage(uint8_t* sensorMessage, uint8_t sensorIdLength) {
	uint8_t* messagePtr = sensorMessage;
	int messageLength = 0;

	while ((*messagePtr) != '\r') {
		messagePtr++;
		messageLength++;
	}

	wchar_t* slicedWideChars = calloc((messageLength - sensorIdLength),
			sizeof(wchar_t));
	int i;

	for (i = 0; i < (messageLength - sensorIdLength); i++) {
		slicedWideChars[i] = btowc(sensorMessage[i + sensorIdLength]);
	}
	slicedWideChars[messageLength - sensorIdLength] = '\0';

	return slicedWideChars;
}
