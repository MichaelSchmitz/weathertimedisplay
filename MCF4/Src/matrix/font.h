/**
 * Copyright (C) 2019 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MATRIX_FONT_H_
#define MATRIX_FONT_H_

#include <stdint.h>
#include <stddef.h>

typedef struct {
	uint8_t value[8];
} Characters8x8;

typedef struct {
	uint8_t value[16];
} Characters16x8;

uint8_t Font_getIndex(wchar_t input);
Characters8x8 Font_getChar8x8(wchar_t input);
Characters16x8 Font_getChar16x8(wchar_t input);

#endif //MATRIX_FONT_H_
