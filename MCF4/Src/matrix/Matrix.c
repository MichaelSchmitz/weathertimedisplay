/**
 * Copyright (C) 2019 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <main.h>
#include "Matrix.h"
#include "definitions.h"
#include "FrameBuffer.h"
#include <stdio.h>
#include <string.h>
#include <wchar.h>

bool bufferReady;
Matrix_mode mode = Weather;
wchar_t temp1[7];
wchar_t temp2[7];
wchar_t hum1[7];
wchar_t hum2[7];
wchar_t pressure1[5];
wchar_t pressure2[5];
wchar_t time[6];
wchar_t date[9];
//Matrix_weatherInfo ?
uint8_t brightness;
wchar_t indoor[7] = L"Indoor\0";
wchar_t outdoor[8] = L"Outdoor\0";
wchar_t indoorShort[3] = L"IN\0";
wchar_t outdoorShort[4] = L"OUT\0";
wchar_t tempUnit[3] = L"°C\0";
wchar_t pressureUnit[5] = L"mBar\0";
Matrix_icon tempIcon1;
Matrix_icon tempIcon2;
Matrix_customLED tempColor1;
Matrix_customLED tempColor2;

static bool Matrix_writeChar(wchar_t input, bool big, uint8_t xPos, uint8_t yPos);
static bool Matrix_writeString(wchar_t* input, uint8_t size, bool big, uint8_t xPos, uint8_t yPos);
static void Matrix_setTemperatureIcon(Matrix_icon *icon, Matrix_customLED *color, float value);

/**
 * Initializes everything the matrix needs to work properly.
 */
void Matrix_init() {
    FrameBuffer_init();
    mode = Weather;
}

/**
 * Sets the Temperature on slot 1.
 * @param temperature wide character array with \0 at index 6.
 */
void Matrix_setTemperature1(wchar_t temperature[7]) {
    wcscpy(temp1, temperature);
    Matrix_setTemperatureIcon(&tempIcon1, &tempColor1, wcstof(temp1, NULL));
}

/**
 * Sets the Temperature on slot 2.
 * @param temperature wide character array with \0 at index 6.
 */
void Matrix_setTemperature2(wchar_t temperature[7]) {
    wcscpy(temp2, temperature);
    Matrix_setTemperatureIcon(&tempIcon2, &tempColor2, wcstof(temp2, NULL));
}

/**
 * Sets the humidity on slot 1.
 * @param humidity wide character array with \0 at index 6.
 */
void Matrix_setHumidity1(wchar_t humidity[7]) {
    wcscpy(hum1, humidity);
}


/**
 * Sets the humidity on slot 2.
 * @param humidity wide character array with \0 at index 6.
 */
void Matrix_setHumidity2(wchar_t humidity[7]) {
    wcscpy(hum2, humidity);
}

/**
 * Sets the pressure on slot 1.
 * @param pressure wide character array with \0 at index 4.
 */
void Matrix_setPressure1(wchar_t pressure[5]) {
	wcscpy(pressure1, pressure);
}

/**
 * Sets the pressure on slot 2.
 * @param pressure wide character array with \0 at index 4.
 */
void Matrix_setPressure2(wchar_t pressure[5]) {
	wcscpy(pressure2, pressure);
}

/**
 * Sets the time.
 * @param t wide character array with \0 at index 5.
 */
void Matrix_setTime(wchar_t t[6]) {
    wcscpy(time, t);
}

/**
 * Sets the date.
 * @param d wide character array with \0 at index 8.
 */
void Matrix_setDate(wchar_t d[9]) {
    wcscpy(date, d);
}

/**
 * Sets the brightness and updates the foreground and background colors accordingly.
 * @param bright value to set the brightness to, between 0 and 100.
 */
void Matrix_setBrightness(uint8_t bright) {
    if (bright >= 0 && bright <= 100) {
        brightness = bright;
    }
    Matrix_LED front = FrameBuffer_getDefaultForeground();
    Matrix_LED back = FrameBuffer_getDefaultBackground();
    Matrix_setForeground(front.red, front.green, front.blue);
    Matrix_setBackground(back.red, back.green, back.blue);
}

/**
 * Set the display mode (enum Matrix_mode).
 */
void Matrix_toggleMode() {
    switch (mode) {
    case TimeAndDate: {
    	mode = Weather;
    	break;
    }
    case Weather: {
    	mode = All;
    	break;
    }
    case All: {
    	mode = WeatherDemo;
    	break;
    }
    case WeatherDemo: {
    	mode = TimeAndDate;
    	break;
    }
    }
}

/**
 * Set the foreground (default and brightness scaled).
 */
void Matrix_setForeground(uint8_t r, uint8_t g, uint8_t b) {
	float brightnessPercent = (brightness / 100.0);
    FrameBuffer_setDefaultForeground(r, g, b);
    FrameBuffer_setForeground(r * brightnessPercent, g * brightnessPercent, b * brightnessPercent);
}

/**
 * Set the background (default and brightness scaled).
 */
void Matrix_setBackground(uint8_t r, uint8_t g, uint8_t b) {
	float brightnessPercent = (brightness / 100.0);
    FrameBuffer_setDefaultBackground(r, g, b);
    FrameBuffer_setBackground(r * brightnessPercent, g * brightnessPercent, b * brightnessPercent);
}

/**
 * Triggers a redraw of the buffer, uses the current mode and variables.
 * @return success of operation.
 */
bool Matrix_redraw() {
    bool result = true;
    switch (mode) {
        case TimeAndDate: {
            result = result & Matrix_writeString(time, 5, true, 12, 8);
            result = result & Matrix_writeString(date, 8, true, 0, 40);
            break;
        }
        case Weather: {
            result = result & Icon_writeIcon(tempIcon1, 0, 0, white, tempColor1, brightness);
            result = result & Matrix_writeString(indoor, 6, false, 12, 0);
            result = result & Matrix_writeString(temp1, 6, false, 0, 8);
            result = result & Matrix_writeString(tempUnit, 2, false, 48, 8);
            result = result & Icon_writeIcon(Humidity, 0, 16, blue, white, brightness);
            result = result & Matrix_writeString(hum1, 6, false, 8, 16);
            result = result & Matrix_writeChar('%', false, 56, 16);
            result = result & Matrix_writeString(pressure1, 4, false, 0, 24);
            result = result & Matrix_writeString(pressureUnit, 4, false, 32, 24);
            result = result & Matrix_writeString(outdoor, 7, false, 8, 32);
            result = result & Icon_writeIcon(tempIcon2, 0, 32, white, tempColor2, brightness);
            result = result & Matrix_writeString(temp2, 6, false, 0, 40);
            result = result & Matrix_writeString(tempUnit, 2, false, 48, 40);
            result = result & Icon_writeIcon(Humidity, 0, 48, blue, white, brightness);
            result = result & Matrix_writeString(hum2, 6, false, 8, 48);
            result = result & Matrix_writeChar('%', false, 56, 48);
            result = result & Matrix_writeString(pressure2, 4, false, 0, 56);
            result = result & Matrix_writeString(pressureUnit, 4, false, 32, 56);
            break;
        }
        case All: {
            result = result & Matrix_writeString(time, 5, false, 12, 0);
            result = result & Matrix_writeString(date, 8, false, 0, 8);
            result = result & Icon_writeIcon(tempIcon1, 20, 16, white, tempColor1, brightness);
            result = result & Matrix_writeString(indoorShort, 2, false, 36, 16);
            result = result & Matrix_writeString(temp1, 6, false, 0, 24);
            result = result & Matrix_writeString(tempUnit, 2, false, 48, 24);
            result = result & Icon_writeIcon(Humidity, 0, 32, blue, white, brightness);
            result = result & Matrix_writeString(hum2, 6, false, 8, 32);
            result = result & Matrix_writeChar('%', false, 56, 32);
            result = result & Icon_writeIcon(tempIcon2, 20, 40, white, tempColor2, brightness);
            result = result & Matrix_writeString(outdoorShort, 3, false, 36, 40);
            result = result & Matrix_writeString(temp2, 6, false, 0, 48);
            result = result & Matrix_writeString(tempUnit, 2, false, 48, 48);
            result = result & Icon_writeIcon(Humidity, 0, 56, blue, white, brightness);
            result = result & Matrix_writeString(hum2, 6, false, 8, 56);
            result = result & Matrix_writeChar('%', false, 56, 56);
            break;
        }
        case WeatherDemo: {
        	result = result & Icon_writeIcon(Humidity, 4, 4, blue, white, brightness);
        	result = result & Icon_writeIcon(ThermometerNormal, 16, 4, white, white, brightness);
        	result = result & Icon_writeIcon(ThermometerCold, 28, 4, white, blue, brightness);
        	result = result & Icon_writeIcon(ThermometerWarm, 4, 16, white, red, brightness);
        	result = result & Icon_writeIcon(Sun, 16, 16, yellow, black, brightness);
        	result = result & Icon_writeIcon(Clouds, 28, 16, white, black, brightness);
        	result = result & Icon_writeIcon(Rain, 4, 28, white, blue, brightness);
        	result = result & Icon_writeIcon(Storm, 16, 28, white, yellow, brightness);
        	break;
        }
        default: {
            return false;
        }
    }
    bufferReady = result;
    return result;
}

/**
 * If buffer is marked for swap, swaps the buffer.
 * Shows one complete cycle (with 3 bit color depth) of the buffer.
 */
void Matrix_show() {
    if (bufferReady) {
        FrameBuffer_Swap();
        bufferReady = false;
    }
        // 3 iterations for 3 bit color depth
    for (uint8_t iteration = 0; iteration < 3; iteration++) {
        uint8_t mask = FrameBuffer_getMask(iteration);
        for (uint8_t scanRow = 0; scanRow < Matrix_scanRows; scanRow++) {
            uint16_t index1 = indexRow1(scanRow);
            uint16_t index2 = indexRow2(scanRow);
            for (uint8_t col = 0; col < Matrix_cols; col++) {
                uint8_t data = FrameBuffer_getDataForLEDs(index1 + col, index2 + col, mask);
                HAL_GPIO_WritePin(Matrix_Data_Port, Matrix_Data_Pins, GPIO_PIN_RESET);
                HAL_GPIO_WritePin(Matrix_Data_Port, data, GPIO_PIN_SET);
                HAL_GPIO_WritePin(Matrix_Ctrl_CLK_GPIO_Port, Matrix_Ctrl_CLK_Pin, GPIO_PIN_SET);
                HAL_GPIO_WritePin(Matrix_Ctrl_CLK_GPIO_Port, Matrix_Ctrl_CLK_Pin, GPIO_PIN_RESET);
            }
            HAL_GPIO_WritePin(Matrix_Ctrl_OE_GPIO_Port, Matrix_Ctrl_OE_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(Matrix_Ctrl_ST_GPIO_Port, Matrix_Ctrl_ST_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(Matrix_Ctrl_ST_GPIO_Port, Matrix_Ctrl_ST_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(Matrix_Row_Port, Matrix_Row_Pins, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(Matrix_Row_Port, scanRow, GPIO_PIN_SET);
            HAL_GPIO_WritePin(Matrix_Ctrl_OE_GPIO_Port, Matrix_Ctrl_OE_Pin, GPIO_PIN_RESET);
        }
    }
}

/**
 * Writes a given character in the given size at the given position.
 *
 * @param input wide character to write.
 * @param big bool to select big (16x8) or small (8x8).
 * @param xPos to start character.
 * @param yPos to start character.
 * @return success of operation.
 */
static bool Matrix_writeChar(wchar_t input, bool big, uint8_t xPos, uint8_t yPos) {
    if (big) {
        return FrameBuffer_write16x8(input, xPos, yPos);
    } else {
        return FrameBuffer_write8x8(input, xPos, yPos);
    }
}

/**
 * Writes a given wide character array in the given size at the given position.
 *
 * @param input wide character array to write.
 * @param length of array ( -1 as \0 should not be written).
 * @param big bool to select big (16x8) or small (8x8).
 * @param xPos to start character.
 * @param yPos to start character.
 * @return success of operation.
 */
static bool Matrix_writeString(wchar_t* input, uint8_t length, bool big, uint8_t xPos, uint8_t yPos) {
    bool result = true;
    if (big) {
        for (uint8_t i = 0; i < length; i++){
            result = result & FrameBuffer_write16x8(input[i], xPos + (i*8), yPos);
        }
    } else {
        for (uint8_t i = 0; i < length; i++){
            result = result & FrameBuffer_write8x8(input[i], xPos + (i*8), yPos);
        }
    }
    return result;
}


/**
 * Sets the Icon and Color of the given depending on the given temperatue value.
 *
 * @param icon pointer to the icon to be set.
 * @param color pointer to the color to be set.
 * @param value temperatue value as float.
 */
static void Matrix_setTemperatureIcon(Matrix_icon *icon, Matrix_customLED *color, float value) {
    if (value < 0) {
        *icon = ThermometerCold;
        *color = blue;
    } else if (value > 15) {
        *icon = ThermometerWarm;
        *color = red;
    } else {
        *icon = ThermometerNormal;
        *color = white;
    }
}
