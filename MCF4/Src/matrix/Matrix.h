/**
 * Copyright (C) 2019 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MATRIX_MATRIX_H_
#define MATRIX_MATRIX_H_

#include <stdbool.h>
#include "icon.h"

typedef enum {
    TimeAndDate,
    Weather,
    All,
	WeatherDemo
} Matrix_mode;

void Matrix_init();
void Matrix_setTemperature1(wchar_t temperature[7]);
void Matrix_setTemperature2(wchar_t temperature[7]);
void Matrix_setHumidity1(wchar_t humidity[7]);
void Matrix_setHumidity2(wchar_t humidity[7]);
void Matrix_setPressure1(wchar_t pressure[5]);
void Matrix_setPressure2(wchar_t pressure[5]);
void Matrix_setTime(wchar_t t[6]);
void Matrix_setDate(wchar_t d[9]);
void Matrix_setBrightness(uint8_t bright);
void Matrix_setForeground(uint8_t r, uint8_t g, uint8_t b);
void Matrix_setBackground(uint8_t r, uint8_t g, uint8_t b);
bool Matrix_redraw();
void Matrix_show();
void Matrix_toggleMode();

#endif //MATRIX_MATRIX_H_
