/**
 * Copyright (C) 2019 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MATRIX_FRAMEBUFFER_H_
#define MATRIX_FRAMEBUFFER_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "definitions.h"

#define indexRow1(yPos) (yPos*Matrix_cols)
#define indexRow2(yPos) ((yPos + Matrix_scanRows)*Matrix_cols)

#define iteration1 0b00000001;
#define iteration2 0b00000010;
#define iteration3 0b00000100;

typedef struct {
	uint8_t red :3;
	uint8_t green :3;
	uint8_t blue :3;
} Matrix_LED;

typedef enum {
	white, yellow, blue, red, black
} Matrix_customLED;

void FrameBuffer_setDataForLED(bool foreground, uint8_t xPos, uint8_t yPos);
void FrameBuffer_setDataForLEDCustom(Matrix_customLED color, uint8_t xPos,
		uint8_t yPos, uint8_t brightness);
void FrameBuffer_Swap();
void FrameBuffer_init();
void FrameBuffer_setDefaultForeground(uint8_t red, uint8_t green, uint8_t blue);
void FrameBuffer_setForeground(uint8_t red, uint8_t green, uint8_t blue);
void FrameBuffer_setDefaultBackground(uint8_t red, uint8_t green, uint8_t blue);
void FrameBuffer_setBackground(uint8_t red, uint8_t green, uint8_t blue);
Matrix_LED FrameBuffer_getDefaultForeground();
Matrix_LED FrameBuffer_getDefaultBackground();
bool FrameBuffer_write16x8(wchar_t input, uint8_t xPos, uint8_t yPos);
bool FrameBuffer_write8x8(wchar_t input, uint8_t xPos, uint8_t yPos);
uint8_t FrameBuffer_getDataForLEDs(uint16_t index1, uint16_t index2,
		uint8_t mask);
uint8_t FrameBuffer_getMask(uint8_t iteration);

#endif //MATRIX_FRAMEBUFFER_H_
