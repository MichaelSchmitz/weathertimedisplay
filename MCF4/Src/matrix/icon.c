/**
 * Copyright (C) 2019 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "icon.h"

uint8_t thermometerIcon[8] = { 0x38, 0x28, 0x28, 0x6C, 0x44, 0x44, 0x38, 0x00 };
uint8_t thermometerWarm[8] = { 0x00, 0x10, 0x10, 0x10, 0x38, 0x38, 0x00, 0x00 };
uint8_t thermometerCold[8] = { 0x00, 0x00, 0x00, 0x00, 0x38, 0x38, 0x00, 0x00 };
uint8_t thermometerNormal[8] =
		{ 0x00, 0x00, 0x00, 0x10, 0x38, 0x38, 0x00, 0x00 };
uint8_t humidityIcon[8] = { 0x00, 0x10, 0x38, 0x5C, 0x7C, 0x7C, 0x38, 0x00 };
uint8_t humidityAccent[8] = { 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00 };
uint8_t sun[8] = { 0x10, 0x54, 0x38, 0xFE, 0x38, 0x54, 0x10, 0x00 };
uint8_t cloud[8] = { 0x00, 0x0C, 0x7A, 0x92, 0x82, 0x7C, 0x00, 0x00 };
uint8_t rain[8] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x54, 0x00 };
uint8_t stormCloud[8] = { 0x00, 0x0C, 0x7A, 0x92, 0x82, 0x74, 0x00, 0x00 };
uint8_t thunder[8] = { 0x00, 0x00, 0x00, 0x08, 0x10, 0x08, 0x10, 0x08 };

/**
 * Writes the given icon at given position with given color and brightness into bufferBack.
 *
 * @param icon Icon to write (enum Matrix_icon).
 * @param xPos to start the icon.
 * @param yPos to start the icon.
 * @param mainColor of the icon (enum Matrix_customLED).
 * @param accentColor of the icon (enum Matrix_customLED), set to black if none expected.
 * @param brightness scalefactor of color brightness.
 * @return success or failure of operation.
 */
bool Icon_writeIcon(Matrix_icon icon, uint8_t xPos, uint8_t yPos,
		Matrix_customLED mainColor, Matrix_customLED accentColor, uint8_t brightness) {
	bool result = true;
	switch (icon) {
	case ThermometerNormal: {
		for (uint8_t x = xPos; x < xPos + 8; x++) {
			for (uint8_t y = yPos; y < yPos + 8; y++) {
				if (isBitSet(thermometerIcon[y - yPos], x - xPos))
					FrameBuffer_setDataForLEDCustom(mainColor, x, y,
							brightness);
				else if (isBitSet(thermometerNormal[y - yPos], x - xPos))
					FrameBuffer_setDataForLEDCustom(accentColor, x, y,
							brightness);
				else
					FrameBuffer_setDataForLED(false, x, y);
			}
		}
		break;
	}
	case ThermometerCold: {
		for (uint8_t x = xPos; x < xPos + 8; x++) {
			for (uint8_t y = yPos; y < yPos + 8; y++) {
				for (uint8_t y = yPos; y < yPos + 8; y++) {
					if (isBitSet(thermometerIcon[y - yPos], x - xPos))
						FrameBuffer_setDataForLEDCustom(mainColor, x, y,
								brightness);
					else if (isBitSet(thermometerCold[y - yPos], x - xPos))
						FrameBuffer_setDataForLEDCustom(accentColor, x, y,
								brightness);
					else
						FrameBuffer_setDataForLED(false, x, y);
				}
			}
		}
		break;
	}
	case ThermometerWarm: {
		for (uint8_t x = xPos; x < xPos + 8; x++) {
			for (uint8_t y = yPos; y < yPos + 8; y++) {
				for (uint8_t y = yPos; y < yPos + 8; y++) {
					if (isBitSet(thermometerIcon[y - yPos], x - xPos))
						FrameBuffer_setDataForLEDCustom(mainColor, x, y,
								brightness);
					else if (isBitSet(thermometerWarm[y - yPos], x - xPos))
						FrameBuffer_setDataForLEDCustom(accentColor, x, y,
								brightness);
					else
						FrameBuffer_setDataForLED(false, x, y);
				}
			}
		}
		break;
	}
	case Humidity: {
		for (uint8_t x = xPos; x < xPos + 8; x++) {
			for (uint8_t y = yPos; y < yPos + 8; y++) {
				if (isBitSet(humidityIcon[y - yPos], x - xPos))
					FrameBuffer_setDataForLEDCustom(mainColor, x, y,
							brightness);
				else if (isBitSet(humidityAccent[y - yPos], x - xPos))
					FrameBuffer_setDataForLEDCustom(accentColor, x, y,
							brightness);
				else
					FrameBuffer_setDataForLED(false, x, y);
			}
		}
		break;
	}
	case Sun: {
		for (uint8_t x = xPos; x < xPos + 8; x++) {
			for (uint8_t y = yPos; y < yPos + 8; y++) {
				if (isBitSet(sun[y - yPos], x - xPos))
					FrameBuffer_setDataForLEDCustom(mainColor, x, y,
							brightness);
				else
					FrameBuffer_setDataForLED(false, x, y);
			}
		}
		break;
	}
	case Clouds: {
		for (uint8_t x = xPos; x < xPos + 8; x++) {
			for (uint8_t y = yPos; y < yPos + 8; y++) {
				if (isBitSet(cloud[y - yPos], x - xPos))
					FrameBuffer_setDataForLEDCustom(mainColor, x, y,
							brightness);
				else
					FrameBuffer_setDataForLED(false, x, y);
			}
		}
		break;
	}
	case Rain: {
		for (uint8_t x = xPos; x < xPos + 8; x++) {
			for (uint8_t y = yPos; y < yPos + 8; y++) {
				if (isBitSet(cloud[y - yPos], x - xPos))
					FrameBuffer_setDataForLEDCustom(mainColor, x, y,
							brightness);
				else if (isBitSet(rain[y - yPos], x - xPos))
					FrameBuffer_setDataForLEDCustom(accentColor, x, y,
							brightness);
				else
					FrameBuffer_setDataForLED(false, x, y);
			}
		}
		break;
	}
	case Storm: {
		for (uint8_t x = xPos; x < xPos + 8; x++) {
			for (uint8_t y = yPos; y < yPos + 8; y++) {
				if (isBitSet(stormCloud[y - yPos], x - xPos))
					FrameBuffer_setDataForLEDCustom(mainColor, x, y,
							brightness);
				else if (isBitSet(thunder[y - yPos], x - xPos))
					FrameBuffer_setDataForLEDCustom(accentColor, x, y,
							brightness);
				else
					FrameBuffer_setDataForLED(false, x, y);
			}
		}
		break;
	}
	default: {
		return false;
	}
	}
	return result;
}
