/**
 * Copyright (C) 2019 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MATRIX_DEFINITIONS_H_
#define MATRIX_DEFINITIONS_H_

#define Matrix_Data_Port GPIOC
#define Matrix_Data_Pins Matrix_R1_Pin | Matrix_G1_Pin | Matrix_B1_Pin | Matrix_R2_Pin | Matrix_G2_Pin | Matrix_B2_Pin
#define Matrix_Row_Port GPIOD
#define Matrix_Row_Pins Matrix_Ctrl_A_Pin | Matrix_Ctrl_B_Pin | Matrix_Ctrl_C_Pin | Matrix_Ctrl_D_Pin | Matrix_Ctrl_E_Pin
#define Matrix_cols 64
#define Matrix_scanRows 32
#define Matrix_rows 64

#define isBitSet(var,pos) ((var) & (0b10000000>>(pos)))

#endif //MATRIX_DEFINITIONS_H_
