/**
 * Copyright (C) 2019 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MATRIX_ICON_H_
#define MATRIX_ICON_H_

#include <stdbool.h>
#include <stdint.h>
#include "FrameBuffer.h"

typedef enum {
    Humidity,
    ThermometerNormal,
    ThermometerCold,
    ThermometerWarm,
    Sun,
    Clouds,
    Rain,
    Storm
} Matrix_icon;

bool Icon_writeIcon(Matrix_icon icon, uint8_t xPos, uint8_t yPos, Matrix_customLED mainColor, Matrix_customLED accentColor, uint8_t brightness);

#endif //MATRIX_ICON_H_
