/**
 * Copyright (C) 2019 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "FrameBuffer.h"
#include "font.h"
#include <string.h>

Matrix_LED buffer[Matrix_cols * Matrix_rows] = { 0 };
Matrix_LED bufferBack[Matrix_cols * Matrix_rows] = { 0 };
Matrix_LED front = { 7, 7, 7 };
Matrix_LED frontDefault = { 7, 7, 7 };
Matrix_LED back = { 0, 0, 0 };
Matrix_LED backDefault = { 0, 0, 0 };
const Matrix_LED whiteColor = { 7, 7, 7 };
const Matrix_LED redColor = { 7, 0, 0 };
const Matrix_LED blueColor = { 0, 0, 7 };
const Matrix_LED yellowColor = { 7, 7, 0 };
const Matrix_LED blackColor = { 0, 0, 0 };

static void FrameBuffer_reset();

/**
 * Sets the given LED to foreground or background color.
 *
 * @param foreground true, if foreground color is to set, otherwise false.
 * @param xPos of LED to set.
 * @param yPos of LED to set.
 */
void FrameBuffer_setDataForLED(bool foreground, uint8_t xPos, uint8_t yPos) {
	Matrix_LED tmp;
	if (foreground) {
		tmp = front;
	} else {
		tmp = back;
	}
	bufferBack[indexRow1(yPos) + xPos] = tmp;
}

/**
 * Sets the given LED to the given color and brightness.
 *
 * @param color enum of type customLED, selects the color.
 * @param xPos of LED to set.
 * @param yPos of LED to set.
 * @param brightness value between 0 and 100 to scale the brightness.
 */
void FrameBuffer_setDataForLEDCustom(Matrix_customLED color, uint8_t xPos,
		uint8_t yPos, uint8_t brightness) {
	Matrix_LED led;
	switch (color) {
	case white: {
		led = whiteColor;
		break;
	}
	case red: {
		led = redColor;
		break;
	}
	case blue: {
		led = blueColor;
		break;
	}
	case black: {
		led = blackColor;
		break;
	}
	case yellow: {
		led = yellowColor;
		break;
	}
	default: {
		led = blackColor;
	}
	}
	float brightnessPercent = brightness / 100.0;
	led.red = led.red * brightnessPercent;
	led.green = led.green * brightnessPercent;
	led.blue = led.blue * brightnessPercent;
	bufferBack[indexRow1(yPos) + xPos] = led;
}

/**
 * Swaps the bufferBack with the buffer and resets the bufferBack.
 */
void FrameBuffer_Swap() {
	uint16_t size = sizeof(Matrix_LED) * Matrix_cols * Matrix_rows;
	memcpy(&buffer, &bufferBack, size);
	FrameBuffer_reset();
}

/**
 * Initializes the buffer and bufferBack with the background color.
 */
void FrameBuffer_init() {
	for (int i = 0; i < Matrix_rows; i++) {
		for (int j = 0; j < Matrix_cols; j++) {
			FrameBuffer_setDataForLED(false, i, j);
			buffer[indexRow1(i) + j] = back;
		}
	}
}

/**
 * Sets the default foreground.
 * Set always the full value, not scaled to brightness.
 *
 * @param red, range from 0 to 7.
 * @param green, range from 0 to 7.
 * @param blue, range from 0 to 7.
 */
void FrameBuffer_setDefaultForeground(uint8_t red, uint8_t green, uint8_t blue) {
	frontDefault.red = red;
	frontDefault.green = green;
	frontDefault.blue = blue;
}

/**
 * Sets the foreground, values should be scaled with brightness.
 *
 * @param red, range from 0 to 7.
 * @param green, range from 0 to 7.
 * @param blue, range from 0 to 7.
 */
void FrameBuffer_setForeground(uint8_t red, uint8_t green, uint8_t blue) {
	front.red = red;
	front.green = green;
	front.blue = blue;
}

/**
 * Sets the default background.
 * Set always the full value, not scaled to brightness.
 *
 * @param red, range from 0 to 7.
 * @param green, range from 0 to 7.
 * @param blue, range from 0 to 7.
 */
void FrameBuffer_setDefaultBackground(uint8_t red, uint8_t green, uint8_t blue) {
	backDefault.red = red;
	backDefault.green = green;
	backDefault.blue = blue;
}

/**
 * Sets the background, values should be scaled with brightness.
 *
 * @param red, range from 0 to 7.
 * @param green, range from 0 to 7.
 * @param blue, range from 0 to 7.
 */
void FrameBuffer_setBackground(uint8_t red, uint8_t green, uint8_t blue) {
	back.red = red;
	back.green = green;
	back.blue = blue;
	FrameBuffer_reset();
}

/**
 * Queries the default foreground.
 * @return LED foregrounDefault.
 */
Matrix_LED FrameBuffer_getDefaultForeground() {
	return frontDefault;
}

/**
 * Queries the default background.
 * @return LED backgroundDefault.
 */
Matrix_LED FrameBuffer_getDefaultBackground() {
	return backDefault;
}

/**
 * Write a complete character in 16x8 at given position into bufferBack.
 *
 * @param input wide character to write.
 * @param xPos to start the character.
 * @param yPos to start the character.
 * @return success of operation.
 */
bool FrameBuffer_write16x8(wchar_t input, uint8_t xPos, uint8_t yPos) {
	if (Matrix_cols - xPos >= 8 && Matrix_rows - yPos >= 16) {
		Characters16x8 character16X8 = Font_getChar16x8(input);
		for (uint8_t y = yPos; y < yPos + 16; y++) {
			for (uint8_t x = xPos; x < xPos + 8; x++) {
				FrameBuffer_setDataForLED(
						isBitSet(character16X8.value[y - yPos], x - xPos), x,
						y);
			}
		}
		return true;
	}
	return false;
}

/**
 * Write a complete character in 8x8 at given position into bufferBack.
 *
 * @param input wide character to write.
 * @param xPos to start the character.
 * @param yPos to start the character.
 * @return sucess of operation.
 */
bool FrameBuffer_write8x8(wchar_t input, uint8_t xPos, uint8_t yPos) {
	if (Matrix_cols - xPos >= 8 && Matrix_rows - yPos >= 8) {
		Characters8x8 character8X8 = Font_getChar8x8(input);
		for (uint8_t y = yPos; y < yPos + 8; y++) {
			for (uint8_t x = xPos; x < xPos + 8; x++) {
				FrameBuffer_setDataForLED(
						isBitSet(character8X8.value[y - yPos], x - xPos), x, y);
			}
		}
		return true;
	}
	return false;
}

/**
 * Queries the data set for a LED at given position in the buffer and merges the data into one single uint8_t.
 *
 * @param index1 position of first LED.
 * @param index2 position of second LED.
 * @return uint8_t with data for the first LED on position 0-2 and for the second on 3-5 (from right).
 */
uint8_t FrameBuffer_getDataForLEDs(uint16_t index1, uint16_t index2,
		uint8_t mask) {
	Matrix_LED led1 = buffer[index1];
	Matrix_LED led2 = buffer[index2];
	uint8_t shiftToPos = mask >> 1;
	uint8_t data = ((mask & led1.red) >> shiftToPos);
	data = data | (((mask & led1.green) >> shiftToPos) << 1);
	data = data | (((mask & led1.blue) >> shiftToPos) << 2);
	data = data | (((mask & led2.red) >> shiftToPos) << 3);
	data = data | (((mask & led2.green) >> shiftToPos) << 4);
	data = data | (((mask & led2.blue) >> shiftToPos) << 5);
	return data;
}

/**
 * Queries the mask to select relevant bits for current iteration.
 *
 * @param iteration value of current iteration.
 * @return bitmask for relevant bits.
 */
uint8_t FrameBuffer_getMask(uint8_t iteration) {
	switch (iteration) {
	case 0:
		return iteration1
	case 1:
		return iteration2
	case 2:
		return iteration3
	default:
		return 0;
	}
}

/**
 * Sets the complete bufferBack to the background color.
 */
static void FrameBuffer_reset() {
	for (int i = 0; i < Matrix_rows; i++) {
		for (int j = 0; j < Matrix_cols; j++) {
			FrameBuffer_setDataForLED(false, i, j);
		}
	}
}
