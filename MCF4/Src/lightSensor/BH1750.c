/**
 * Copyright (C) 2019 Johannes Klüpfel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "BH1750.h"

uint8_t rawDataBuffer[2];

/**
 * Initializes the BH1750 Sensor.
 * @param I2C_HandleTypeDef.
 * @return Success indicator.
 */
bool BH1750_init(I2C_HandleTypeDef *hi2c) {
	HAL_Delay(200);
	uint8_t mode = BH1750_CONTINUOUS_HIGH_RES_MODE;
	if(HAL_I2C_Master_Transmit(hi2c, BH1750_I2CADDR, &mode , 1, HAL_MAX_DELAY) != HAL_OK){
			return false;
	}
	HAL_Delay(10);
	return true;
}

/**
 * Reads the Light intensity from Sensor.
 * @param I2C_HandleTypeDef.
 * @return Light intensity in lux.
 */
float BH1750_readIntensity(I2C_HandleTypeDef *hi2c) {
	rawDataBuffer[0]=0;
	rawDataBuffer[1]=0;
	if(	HAL_I2C_Master_Receive(hi2c, BH1750_I2CADDR, rawDataBuffer, 2, HAL_MAX_DELAY) != HAL_OK){
		return -1;
	}

	//Success
	float result = -1;
	result = (rawDataBuffer[0]<<8|rawDataBuffer[1])/1.2;
	return result;
}

/**
 * Reads the light intensity and categorizes it into 4 categories.
 * @param I2C_HandleTypeDef.
 * @return Categorized light intensity.
 */
uint8_t GetLightCategory(I2C_HandleTypeDef *hi2c) {
	float lightInten = BH1750_readIntensity(hi2c);

	if(lightInten < 10.0){
		//dark
		return 0;
	}else if(lightInten < 100.0){
		//dawn light
		return 1;
	}else if(lightInten < 500.0 ){
		//normal light
		return 2;
	}else{
		return 3;
	}
}
