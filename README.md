This project was created as a project for the course 'Programming of microcontrollers' at the Technische Hochschule Nürnberg Georg Simon Ohm.

# Hardware
- BME280 for temperature and humidity
- BH1750 for light level detection
- atom time reciever for automatic clock adjusting
- 2x ESP8266 (Nodemcu)
- HUB75E LED Matrix (P2.5-64x64 160mmx160mm)
- STM32 G0 Microcontroller
- STM32 F4 Microcontroller
- some buttons, cables and breadboards
- 90W 5V 18A power supply

## Connecting
- F4 connected to Matrix (PIN C0-12, G0), Brightness Sensor (PIN B8, B9, PA3 (Power), GND(x2)), ESP (PIN D6 for RX; 3.3V and GND)
- G0 connected to two temperature sensor (PIN B9 and B8; A12 and A11; 3.3V and GND), Range Sensor (Ultrasound) (PIN ), Time Source (PIN x), ESP (PIN C4 for TX; 3.3V and GND) 

# Features
## Matrix (Model: HUB75E)
completly reverse engineered as no documentation was found. Can display time, temperature, humidity and weather. Weather is only a demo feature as no weather data is collected. Color of text and background can be cycled as well as modes.

Matrix uses 1/32 scan mode with 120fps and is normally used with expensive video controllers able to show hdmi input etc.


## Atom Time
Synchronisation is performed automatically, due to the nature of the signal (1min intervalls) allow some time for synchronisation

## Temperature/Humidity/Brightness
I2C is used to get readings from the BME280/BH1750. Transfer via WIFI from G0 to the F4.

## Wifi
ESP8266 are used to transfer input on the serial pins via wifi to the other ESP in the network.


## Links to different guides
- [Matrix](https://learn.adafruit.com/32x16-32x32-rgb-led-matrix?view=all)
- [Atom-Time-Reciever](http://m.arduino-projekte.webnode.at/meine-projekte/dcf77-empfaenger/)
- [Light Sensor](https://www.aeq-web.com/bh1750-luxmeter-fuer-arduino/)
- [BME280 Temperature Humidity](https://www.makershop.de/bme280-sensor-arduino-nano/)

# Authors
- Lukas Welsch
- Phillip Nendel
- Johannes Klüpfel
- Michael Schmitz
