#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#define STASSID "SSID"
#define STAPSK  "Password"

IPAddress recipientIP(192, 168, 43, 225);
unsigned int localPort = 11111;
unsigned int recipientPort = 11111;

WiFiUDP Udp;
String data;

void setup() {
  Serial.begin(115200);

  WiFi.disconnect();

  WiFi.mode(WIFI_STA);
  WiFi.hostname("NodeMCU_sender");
  WiFi.begin(STASSID, STAPSK);
  
  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("Connecting...");
    delay(500);
  }
  Serial.print("Connected! IP address: ");
  Serial.println(WiFi.localIP());
  Serial.printf("UDP server on port %d\n", localPort);
  Udp.begin(localPort);
}

void loop() {

    if (Serial.available() > 0) {
        readAndTransmitDataOverWifi();
    }
}

void readAndTransmitDataOverWifi()
{
    data = Serial.readStringUntil('\n');
    Udp.beginPacket(recipientIP, recipientPort);
    Udp.print(data);
    Udp.endPacket();
}
