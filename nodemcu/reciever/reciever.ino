#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#define STASSID "SSID"
#define STAPSK  "Password"

IPAddress ip(192, 168, 43, 225);
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 255, 0);
unsigned int localPort = 11111;

char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1];

WiFiUDP Udp;

void setup() {
  Serial.begin(115200);

  WiFi.disconnect();

  WiFi.mode(WIFI_STA);
  WiFi.config(ip, gateway, subnet);
  WiFi.begin(STASSID, STAPSK);
  
  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("Connecting...");
    delay(1000);
  }
  Serial.print("Connected! IP address: ");
  Serial.println(WiFi.localIP());
  Serial.printf("UDP server on port %d\n", localPort);
  Udp.begin(localPort);
}

void loop() {
    receiveAndRetransmitUdpPackets();
}

void receiveAndRetransmitUdpPackets()
{
   int packetSize = Udp.parsePacket();
    if (packetSize > 0) 
   {
        int n = Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
        packetBuffer[n] = 0;
        Serial.println(packetBuffer);
   }
}
