/**
 * Copyright (C) 2019 Johannes Klüpfel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "BME280.h"
#include <stdio.h>

static int write_register8(BME280_HandleTypedef *dev, uint8_t addr,	uint8_t value);
static bool read_register16(BME280_HandleTypedef *dev, uint8_t addr, uint16_t *value);
static inline int read_data(BME280_HandleTypedef *dev, uint8_t addr, uint8_t *value, uint8_t len);
static inline int32_t compensate_temperature(BME280_HandleTypedef *dev, int32_t adc_temp, int32_t *fine_temp);
static inline uint32_t compensate_pressure(BME280_HandleTypedef *dev, int32_t adc_press, int32_t fine_temp);
static inline uint32_t compensate_humidity(BME280_HandleTypedef *dev, int32_t adc_hum, int32_t fine_temp);

/**
* Initializes BMP Sensor in Forced Mode with no Filter and 1x Oversampling.
* @param BME280_HandleTypedef.
*/
bool BME280_Initialise(BME280_HandleTypedef *dev) {
	// Check for Valid Device Addresses
	if (dev->addr != 0x76 && dev->addr != 0x77) {
		return false;
	}

	// SoftReset (Write Reset Value to Reset Register)
	if (!write_register8(dev, BMP280_REG_RESET, BMP280_RESET_VALUE)) {
		return false;
	}

	// Wait for Reset
	while (1) {
		uint8_t status;
		if (read_data(dev, BMP280_REG_STATUS, &status, 1) && (status & 1) == 0)
			break;
	}

	if (!read_calibration_data(dev)) {
		return false;
	}

	// Configure the Sensor
	// Standby 5 to left, Filter 2 to left
	uint8_t config = (5 << 5) | (0 << 2);
	if (!write_register8(dev, BMP280_REG_CONFIG, config)) {
		return false;
	}

	// Oversampling Temperature, Oversampling Pressure, Mode
	uint8_t ctrl = (1 << 5) | (1 << 2) | (0);
	//Oversampling Humidity
	uint8_t ctrl_hum = 1;

	// Write ctrl_hum first
	if (!write_register8(dev, BMP280_REG_CTRL_HUM, ctrl_hum)) {
		return false;
	}

	// Then write ctrl Register to let the Changes Take Place
	if (!write_register8(dev, BMP280_REG_CTRL, ctrl)) {
		return false;
	}
	// If everything was successful
	return true;
}

/**
* Reads the Calibration Data from Sensor and saves it to the Sensor Handle.
* @param BME280_HandleTypedef.
* @return Status indicator.
*/
bool read_calibration_data(BME280_HandleTypedef *dev) {

	uint16_t h4, h5;

	if (read_register16(dev, 0x88, &dev->dig_T1)
			&& read_register16(dev, 0x8a, (uint16_t *) &dev->dig_T2)
			&& read_register16(dev, 0x8c, (uint16_t *) &dev->dig_T3)
			&& read_register16(dev, 0x8e, &dev->dig_P1)
			&& read_register16(dev, 0x90, (uint16_t *) &dev->dig_P2)
			&& read_register16(dev, 0x92, (uint16_t *) &dev->dig_P3)
			&& read_register16(dev, 0x94, (uint16_t *) &dev->dig_P4)
			&& read_register16(dev, 0x96, (uint16_t *) &dev->dig_P5)
			&& read_register16(dev, 0x98, (uint16_t *) &dev->dig_P6)
			&& read_register16(dev, 0x9a, (uint16_t *) &dev->dig_P7)
			&& read_register16(dev, 0x9c, (uint16_t *) &dev->dig_P8)
			&& read_register16(dev, 0x9e, (uint16_t *) &dev->dig_P9)
			&& read_data(dev, 0xa1, &dev->dig_H1, 1)
			&& read_register16(dev, 0xe1, (uint16_t *) &dev->dig_H2)
			&& read_data(dev, 0xe3, &dev->dig_H3, 1)
			&& read_register16(dev, 0xe4, &h4)
			&& read_register16(dev, 0xe5, &h5)
			&& read_data(dev, 0xe7, (uint8_t *) &dev->dig_H6, 1)) {

		dev->dig_H4 = (h4 & 0x00ff) << 4 | (h4 & 0x0f00) >> 8;
		dev->dig_H5 = h5 >> 4;
		return true;
	}

	return false;
}

/**
 * Forces the sensor to perform a Measurement. Only in Forced Mode.
 * @param BME280_HandleTypedef.
 * @return Success indicator.
 */
bool BME280_force_measurement(BME280_HandleTypedef *dev) {
	uint8_t ctrl;
	if (!read_data(dev, BMP280_REG_CTRL, &ctrl, 1))
		return false;
	ctrl &= ~0b11;  // clear two lower bits
	ctrl |= 1;
	if (!write_register8(dev, BMP280_REG_CTRL, ctrl)) {
		return false;
	}
	return true;
}

/**
* Tests if the sensor is performing a measurement.
* @param BME280_HandleTypedef.
* @returns measurement status.
*/
bool BME280_is_measuring(BME280_HandleTypedef *dev) {
	uint8_t status;
	if (!read_data(dev, BMP280_REG_STATUS, &status, 1))
		return false;
	if (status & (1 << 3)) {
		return true;
	}
	return false;
}

/**
* Reads the compensated data from Sensor in fixed Format.
* @param BME280_HandleTypedef.
* @param Temperature in degree Celsius * 100.
* @param Pressure in Pascal in fixed point 24 bit integer 8 bit fraction format.
* @param Humidity in a fixed point 22 bit integer and 10 bit fraction format.
* @return Success indicator.
*/
bool BME280_read_fixed(BME280_HandleTypedef *dev, int32_t *temperature,
		uint32_t *pressure, uint32_t *humidity) {
	int32_t adc_pressure;
	int32_t adc_temp;
	int32_t adc_humidity;

	uint8_t data[8];
	int size = 8; //8 following Registers

	if (!read_data(dev, 0xf7, data, size)) {
		return false;
	}

	adc_pressure = data[0] << 12 | data[1] << 4 | data[2] >> 4;
	adc_temp = data[3] << 12 | data[4] << 4 | data[5] >> 4;
	adc_humidity = data[6] << 8 | data[7];

	int32_t fine_temp;
	*temperature = compensate_temperature(dev, adc_temp, &fine_temp);
	*pressure = compensate_pressure(dev, adc_pressure, fine_temp);
	*humidity = compensate_humidity(dev, adc_humidity, fine_temp);

	return true;
}

/**
* Reads compensated temperature, pressure and humidity.
* @param BME280_HandleTypedef.
* @param pointer to a float variable where to store the temperature.
* @param pointer to a float variable where to store the pressure.
* @param pointer to a float variable where to store the humidity.
* @return Success indicator.
*/
bool BME280_read_float(BME280_HandleTypedef *dev, float *temperature,
		float *pressure, float *humidity) {
	int32_t fixed_temperature;
	uint32_t fixed_pressure;
	uint32_t fixed_humidity;
	;

	if (BME280_read_fixed(dev, &fixed_temperature, &fixed_pressure,
			&fixed_humidity)) {
		*temperature = (float) fixed_temperature / 100;
		*pressure = (float) (fixed_pressure / 256) / 100;
		*humidity = (float) fixed_humidity / 1024;
		return true;
	}
	return false;
}

/**
* Writes 1 Register of the BME280 Sensor.
* @param BME280_HandleTypedef.
* @param Address of Register to write.
* @param Value to write into Register.
* @return Status indicator.
*/
static int write_register8(BME280_HandleTypedef *dev, uint8_t addr,
		uint8_t value) {
	uint16_t tx_buff;

	tx_buff = (dev->addr << 1);

	if (HAL_I2C_Mem_Write(dev->i2c, tx_buff, addr, 1, &value, 1, 10000)
			== HAL_OK)
		return true;
	else
		return false;
}

/**
* Reads 16Bit of Data (2 Registers) from the Sensor.
* @param BME280_HandleTypedef.
* @param Address of Register to read from.
* @param Pointer to a variable to store the registervalue.
* @return Status indicator.
*/
static bool read_register16(BME280_HandleTypedef *dev, uint8_t addr,
		uint16_t *value) {
	uint16_t tx_buff;
	uint8_t rx_buff[2];
	tx_buff = (dev->addr << 1);

	if (HAL_I2C_Mem_Read(dev->i2c, tx_buff, addr, 1, rx_buff, 2, 5000)
			== HAL_OK) {
		*value = (uint16_t) ((rx_buff[1] << 8) | rx_buff[0]);
		return true;
	} else
		return false;

}
/**
* Reads Data from the Sensor.
* @param BME280_HandleTypedef.
* @param Address of Register to read from.
* @param Pointer to a variable to store the registervalue.
* @param Count of consecutive Registers to read.
* @return Status indicator.
*/
static inline int read_data(BME280_HandleTypedef *dev, uint8_t addr,
		uint8_t *value, uint8_t len) {
	uint16_t tx_buff;
	tx_buff = (dev->addr << 1);
	if (HAL_I2C_Mem_Read(dev->i2c, tx_buff, addr, 1, value, len, 5000)
			== HAL_OK)
		return true;
	else
		return false;

}

/**
 * Compensation Algorithm from datasheet. Return Value is in Degree Celsius.
 */
static inline int32_t compensate_temperature(BME280_HandleTypedef *dev,
		int32_t adc_temp, int32_t *fine_temp) {
	int32_t var1, var2;

	var1 = ((((adc_temp >> 3) - ((int32_t) dev->dig_T1 << 1)))
			* (int32_t) dev->dig_T2) >> 11;
	var2 = (((((adc_temp >> 4) - (int32_t) dev->dig_T1)
			* ((adc_temp >> 4) - (int32_t) dev->dig_T1)) >> 12)
			* (int32_t) dev->dig_T3) >> 14;

	*fine_temp = var1 + var2;
	return (*fine_temp * 5 + 128) >> 8;
}

/**
 * Compensation Algorithm from datasheet. Return Value is in Pa.
 */
static inline uint32_t compensate_pressure(BME280_HandleTypedef *dev,
		int32_t adc_press, int32_t fine_temp) {
	int64_t var1, var2, p;

	var1 = (int64_t) fine_temp - 128000;
	var2 = var1 * var1 * (int64_t) dev->dig_P6;
	var2 = var2 + ((var1 * (int64_t) dev->dig_P5) << 17);
	var2 = var2 + (((int64_t) dev->dig_P4) << 35);
	var1 = ((var1 * var1 * (int64_t) dev->dig_P3) >> 8)
			+ ((var1 * (int64_t) dev->dig_P2) << 12);
	var1 = (((int64_t) 1 << 47) + var1) * ((int64_t) dev->dig_P1) >> 33;

	if (var1 == 0) {
		return 0;  // avoid exception caused by division by zero
	}

	p = 1048576 - adc_press;
	p = (((p << 31) - var2) * 3125) / var1;
	var1 = ((int64_t) dev->dig_P9 * (p >> 13) * (p >> 13)) >> 25;
	var2 = ((int64_t) dev->dig_P8 * p) >> 19;

	p = ((p + var1 + var2) >> 8) + ((int64_t) dev->dig_P7 << 4);
	return p;
}

/**
 * Compensation Algorithm from datasheet.
 */
static inline uint32_t compensate_humidity(BME280_HandleTypedef *dev,
		int32_t adc_hum, int32_t fine_temp) {
	int32_t v_x1_u32r;

	v_x1_u32r = fine_temp - (int32_t) 76800;
	v_x1_u32r = ((((adc_hum << 14) - ((int32_t) dev->dig_H4 << 20)
			- ((int32_t) dev->dig_H5 * v_x1_u32r)) + (int32_t) 16384) >> 15)
			* (((((((v_x1_u32r * (int32_t) dev->dig_H6) >> 10)
					* (((v_x1_u32r * (int32_t) dev->dig_H3) >> 11)
							+ (int32_t) 32768)) >> 10) + (int32_t) 2097152)
					* (int32_t) dev->dig_H2 + 8192) >> 14);
	v_x1_u32r = v_x1_u32r
			- (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7)
					* (int32_t) dev->dig_H1) >> 4);
	v_x1_u32r = v_x1_u32r < 0 ? 0 : v_x1_u32r;
	v_x1_u32r = v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r;
	return v_x1_u32r >> 12;
}

