/**
 * Copyright (C) 2019 Johannes Klüpfel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BME280_H_
#define BME280_H_

#include <main.h>
#include <stdint.h>
#include <stdbool.h>

// Register Addresses
#define BMP280_REG_TEMP_XLSB   0xFC /* bits: 7-4 */
#define BMP280_REG_TEMP_LSB    0xFB
#define BMP280_REG_TEMP_MSB    0xFA
#define BMP280_REG_TEMP        (BMP280_REG_TEMP_MSB)
#define BMP280_REG_PRESS_XLSB  0xF9 /* bits: 7-4 */
#define BMP280_REG_PRESS_LSB   0xF8
#define BMP280_REG_PRESS_MSB   0xF7
#define BMP280_REG_PRESSURE    (BMP280_REG_PRESS_MSB)
#define BMP280_REG_CONFIG      0xF5 /* bits: 7-5 t_sb; 4-2 filter; 0 spi3w_en */
#define BMP280_REG_CTRL        0xF4 /* bits: 7-5 osrs_t; 4-2 osrs_p; 1-0 mode */
#define BMP280_REG_STATUS      0xF3 /* bits: 3 measuring; 0 im_update */
#define BMP280_REG_CTRL_HUM    0xF2 /* bits: 2-0 osrs_h; */
#define BMP280_REG_RESET       0xE0
#define BMP280_REG_ID          0xD0
#define BMP280_REG_CALIB       0x88
#define BMP280_REG_HUM_CALIB   0x88

#define BMP280_RESET_VALUE     0xB6

typedef struct {
	/*Temperature Compensation*/
	uint16_t dig_T1;
	int16_t dig_T2;
	int16_t dig_T3;

	/* Pressure compensation*/
	uint16_t dig_P1;
	int16_t dig_P2;
	int16_t dig_P3;
	int16_t dig_P4;
	int16_t dig_P5;
	int16_t dig_P6;
	int16_t dig_P7;
	int16_t dig_P8;
	int16_t dig_P9;

	/* Humidity compensation for BME280 */
	uint8_t dig_H1;
	int16_t dig_H2;
	uint8_t dig_H3;
	int16_t dig_H4;
	int16_t dig_H5;
	int8_t dig_H6;

	uint16_t addr;

	I2C_HandleTypeDef* i2c;

} BME280_HandleTypedef;

bool BME280_Initialise(BME280_HandleTypedef *dev);

bool BME280_read_fixed(BME280_HandleTypedef *dev, int32_t *temperature,
		uint32_t *pressure, uint32_t *humidity);

bool BME280_read_float(BME280_HandleTypedef *dev, float *temperature,
		float *pressure, float *humidity);

bool read_calibration_data(BME280_HandleTypedef *dev);

bool BME280_force_measurement(BME280_HandleTypedef *dev);

bool BME280_is_measuring(BME280_HandleTypedef *dev);

#endif /* BME280_H_ */
