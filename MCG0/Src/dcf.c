/**
 * Copyright (C) 2019 Lukas Welsch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dcf.h"

struct Dcf_time time = { 0 };

/*
 * parses date and time with parity.
 * @param values of last transmission.
 */
bool Dcf_parse(bool values[]) {
	uint8_t bytewert = 0;
	uint8_t parity_cnt = 0;

	// minutes
	if (values[21]) {
		bytewert += 1;
		parity_cnt++;
	}
	if (values[22]) {
		bytewert += 2;
		parity_cnt++;
	}
	if (values[23]) {
		bytewert += 4;
		parity_cnt++;
	}
	if (values[24]) {
		bytewert += 8;
		parity_cnt++;
	}
	if (values[25]) {
		bytewert += 10;
		parity_cnt++;
	}
	if (values[26]) {
		bytewert += 20;
		parity_cnt++;
	}
	if (values[27]) {
		bytewert += 40;
		parity_cnt++;
	}
	if (values[28]) {
		parity_cnt++;

	}
	if ((parity_cnt % 2)) {
		return false;
	}

	time.min = bytewert;
	bytewert = 0;
	parity_cnt = 0;

	// hours
	if (values[29]) {
		bytewert += 1;
		parity_cnt++;
	}
	if (values[30]) {
		bytewert += 2;
		parity_cnt++;
	}
	if (values[31]) {
		bytewert += 4;
		parity_cnt++;
	}
	if (values[32]) {
		bytewert += 8;
		parity_cnt++;
	}
	if (values[33]) {
		bytewert += 10;
		parity_cnt++;
	}
	if (values[34]) {
		bytewert += 20;
		parity_cnt++;
	}
	if (values[35]) {
		parity_cnt++;

	}
	if ((parity_cnt % 2)) {
		return false;
	}
	time.std = bytewert;
	bytewert = 0;
	parity_cnt = 0;

	// day
	if (values[36]) {
		bytewert += 1;
		parity_cnt++;
	}
	if (values[37]) {
		bytewert += 2;
		parity_cnt++;
	}
	if (values[38]) {
		bytewert += 4;
		parity_cnt++;
	}
	if (values[39]) {
		bytewert += 8;
		parity_cnt++;
	}
	if (values[40]) {
		bytewert += 10;
		parity_cnt++;
	}
	if (values[41]) {
		bytewert += 20;
		parity_cnt++;
	}

	time.tag = bytewert;
	bytewert = 0;

	// weekday
	if (values[42]) {
		bytewert += 1;
		parity_cnt++;
	}
	if (values[43]) {
		bytewert += 2;
		parity_cnt++;
	}
	if (values[44]) {
		bytewert += 4;
		parity_cnt++;
	}

	bytewert = 0;

	// month
	if (values[45]) {
		bytewert += 1;
		parity_cnt++;
	}
	if (values[46]) {
		bytewert += 2;
		parity_cnt++;
	}
	if (values[47]) {
		bytewert += 4;
		parity_cnt++;
	}
	if (values[48]) {
		bytewert += 8;
		parity_cnt++;
	}
	if (values[49]) {
		bytewert += 10;
		parity_cnt++;
	}

	time.monat = bytewert;
	bytewert = 0;

	// year
	if (values[50]) {
		bytewert += 1;
		parity_cnt++;
	}
	if (values[51]) {
		bytewert += 2;
		parity_cnt++;
	}
	if (values[52]) {
		bytewert += 4;
		parity_cnt++;
	}
	if (values[53]) {
		bytewert += 8;
		parity_cnt++;
	}
	if (values[54]) {
		bytewert += 10;
		parity_cnt++;
	}
	if (values[55]) {
		bytewert += 20;
		parity_cnt++;
	}
	if (values[56]) {
		bytewert += 40;
		parity_cnt++;
	}
	if (values[57]) {
		bytewert += 80;
		parity_cnt++;
	}

	if (values[58]) {
		parity_cnt++;
	}

	if ((parity_cnt % 2)) {
		return false;
	}
	time.jahr = bytewert;

	return true;
}

struct Dcf_time Dcf_output() {
	return time;
}
